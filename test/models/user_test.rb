require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: "Example User", email: "example@email.it",
                      password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be not empty" do
    @user.name = "     "
    assert_not @user.valid?
  end
  
  test "email should be not empty" do
    @user.email = "    "
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.org"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.org USER@foo.COM A_US-ER@foo.bar.org 
                          fisrt.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,org user_at_foo.org user.name@example. 
                            foo@bar_baz.com foo@bar+baz.com caz@dsd..das]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.save
  end
  
  test "email addresses should be saved as lowe-case" do 
    mixed_case_email = "FoObAr@GmAiL.cOm"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.email
  end
  
  test "password should be presente" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false fo a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end
  
  test "associated micropost should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
  
  test "should follow and unfollow user" do
    endriu = users(:endriu)
    simo = users(:simo)
    assert_not endriu.following?(simo)
    endriu.follow(simo)
    assert endriu.following?(simo)
    assert simo.followers.include?(endriu)
    endriu.unfollow(simo)
    assert_not endriu.following?(simo)
  end
  
  test "feed should show only follower and user microposts" do
    endriu = users(:endriu)
    simo = users(:simo)
    thelonini = users(:thelonini)
    #Followed user
    thelonini.microposts.each do |post_following|
      assert endriu.feed.include?(post_following)
    end
    #Non followes user
    simo.microposts.each do |post_unfollowed|
      assert_not endriu.feed.include?(post_unfollowed)
    end
    #User micropost in feed
    endriu.microposts.each do |post_self|
      assert endriu.feed.include?(post_self)
    end
  end

end
